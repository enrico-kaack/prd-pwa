import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
require('vue-material/dist/vue-material.css')

Vue.use(Router)
var VueMaterial = require('vue-material')
Vue.use(VueMaterial)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    }
  ]
})
